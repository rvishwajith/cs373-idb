# cs373-idb

Public repository for CS N373 Project 2.

## Group Members

- Nick Pannell (Group leader)
- Rohith Vishwajith
- Alex Chavez
- Hai Hoang
- Jerry Ehimuh
- Kaden Kirkland

## Estimated Time to Completion

For the design phase, the estimated time-to-completion is 5 days.
